from knowledge_integrity.models.revertrisk_multilingual.model import (
    diff_lines,
    get_edit_info,
    one_hot_encode,
)


def test_diff_lines() -> None:
    old = "Hello\nworld"
    new = "Helo\nit's\na\nsunny\nday"

    diff = diff_lines(old, new)

    for attr, expected in [
        (sorted(diff.added), ["a", "day", "it's", "sunny"]),
        (sorted(diff.deleted), ["world"]),
        (sorted(diff.changed), [("Hello", "Helo")]),
    ]:
        assert attr == expected


def test_get_edit_info() -> None:
    prev_wikitext = """This is a lead.
== Section I ==
Sec I body.
=== Section I.A ===
Section I.A [[body]].
"""
    curr_wikitext = """This is a lead.
== Section I ==
Sec I body. {{and a|template}}
== Section II ==
=== Section II.A ===
Section II.A body.
"""
    edit_info = get_edit_info(prev_wikitext, curr_wikitext, "en")

    for action_type, count in [
        ("remove_Wikilink", 1),
        ("insert_Template", 1),
        ("insert_Heading", 1),
        ("change_Heading", 1),
        ("change_Word", 1),
        ("change_Sentence", 1),
        ("change_Paragraph", 1),
        ("change_Section", 2),
        ("insert_Section", 1),
        ("insert_Whitespace", 2),
    ]:
        assert edit_info.actions[action_type] == count

    for attr, expected in [
        (sorted(edit_info.text_diff.added), sorted(["", "\nSection II.A body.\n"])),
        (sorted(edit_info.text_diff.deleted), sorted(["Section I", "A body."])),
        (sorted(edit_info.text_diff.changed), sorted([("Sec I body.", "Sec I body")])),
    ]:
        assert attr == expected


def test_one_hot_encode() -> None:
    sample = ["b"]
    labels = ["a", "b", "c"]
    prefix = ""

    encoded_sample = one_hot_encode(sample, labels, prefix=prefix)

    assert encoded_sample == dict(a=0, b=1, c=0)
