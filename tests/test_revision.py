import datetime
import json

from typing import Any, Dict

import jsonschema  # type: ignore
import mwapi  # type: ignore
import pytest
import pytest_asyncio

from knowledge_integrity.revision import (
    Page,
    ParentRevision,
    User,
    build_current_revision,
    get_current_revision,
    get_page,
    get_parent_revision,
    get_user,
)


@pytest.fixture(scope="module")
def vcr_config() -> Dict[str, Any]:
    return dict(
        cassette_library_dir="tests/fixtures/cassettes",
        record_mode="new_episodes",
        match_on=["uri", "method"],
    )


@pytest_asyncio.fixture(scope="module")
async def async_session() -> mwapi.AsyncSession:
    return mwapi.AsyncSession(host="https://en.wikipedia.org")


@pytest.fixture
def revision_data() -> Dict[str, Any]:
    return {
        "rev_id": 1234,
        "rev_bytes": 2800,
        "rev_comment": "Added category baz.",
        "rev_text": """This is a lead.
                == Section I ==
                Section I body. {{and a|template}}
                === Section I.A ===
                Section I.A [[body]].
                === Section I.B ===
                Section I.B body.

                [[Category:bar]]
                [[Category:baz]]
            """,
        "rev_timestamp": "2022-02-15T04:30:00Z",
        "rev_tags": [],
        "parent_id": 1200,
        "parent_bytes": 2600,
        "parent_comment": "Added section I.B",
        "parent_text": """This is a lead.
                == Section I ==
                Section I body. {{and a|template}}
                === Section I.A ===
                Section I.A [[body]].
                === Section I.B ===
                Section I.B body.

                [[Category:bar]]
            """,
        "parent_timestamp": "2021-01-01T02:00:00Z",
        "parent_tags": [],
        "user_id": 4567,
        "user_editcount": 30,
        "user_groups": ["rollbacker"],
        "user_registration_timestamp": "2020-06-01T10:05:05Z",
        "page_id": 1008,
        "page_title": "this is a title",
        "page_first_edit_timestamp": "2018-01-01T10:02:02Z",
        "lang": "en",
    }


@pytest.mark.vcr
@pytest.mark.asyncio
async def test_get_user(async_session: mwapi.AsyncSession) -> None:
    user_id = 4587601
    user = await get_user(async_session, user_id)

    assert user is not None

    for attr, expected in [
        (user.id, user_id),
        (user.groups, ["*", "user", "autoconfirmed"]),
        (user.editcount, 393),
        (user.is_anonymous, False),
        (
            user.registration_timestamp,
            datetime.datetime(2007, 6, 7, 16, 36, 3, tzinfo=datetime.timezone.utc),
        ),
    ]:
        assert attr == expected


@pytest.mark.vcr
@pytest.mark.asyncio
async def test_get_user_anonymous(async_session: mwapi.AsyncSession) -> None:
    user_id = 0
    user = await get_user(async_session, user_id)

    assert user is not None

    for attr, expected in [
        (user.id, user_id),
        (user.groups, []),
        (user.editcount, 0),
    ]:
        assert attr == expected

    for attr, expected in [
        (user.is_anonymous, True),
        (user.registration_timestamp, None),
    ]:
        assert attr is expected


@pytest.mark.vcr
@pytest.mark.asyncio
async def test_get_page(async_session: mwapi.AsyncSession) -> None:
    page_id = 211652
    page = await get_page(async_session, page_id)

    assert page is not None
    assert page.id == page_id
    assert page.first_edit_timestamp == datetime.datetime(
        2003, 4, 16, 22, 7, 27, tzinfo=datetime.timezone.utc
    )
    assert page.title == "Constraint satisfaction problem"


@pytest.mark.vcr
@pytest.mark.asyncio
async def test_get_parent_revision(async_session: mwapi.AsyncSession) -> None:
    lang = "en"
    rev_id = 1094938018
    parent = await get_parent_revision(async_session, rev_id, lang)

    assert parent is not None

    for attr, expected in [
        (parent.id, rev_id),
        (parent.bytes, 20464),
        (parent.lang, lang),
        (parent.comment, ""),
        (
            parent.timestamp,
            datetime.datetime(2022, 6, 25, 13, 1, 2, tzinfo=datetime.timezone.utc),
        ),
    ]:
        assert attr == expected

    for fn, arg, ret in [
        (len, parent.categories, 2),
        (len, parent.headings, 17),
        (len, parent.media, 18),
        (len, parent.links, 90),
        (len, parent.wikilinks, 76),
        (len, parent.references, 7),
        (len, parent.tags, 4),
    ]:
        assert fn(arg) == ret


@pytest.mark.vcr
@pytest.mark.asyncio
async def test_get_current_revision(async_session: mwapi.AsyncSession) -> None:
    lang = "en"
    rev_id = 1095483351
    revision = await get_current_revision(async_session, rev_id, lang)

    assert revision is not None

    for attr, expected in [
        (revision.id, rev_id),
        (revision.bytes, 20533),
        (revision.lang, lang),
        (revision.comment, "Added {{Manifolds}}"),
        (
            revision.timestamp,
            datetime.datetime(2022, 6, 28, 16, 48, 52, tzinfo=datetime.timezone.utc),
        ),
    ]:
        assert attr == expected

    for fn, arg, ret in [
        (len, revision.categories, 4),
        (len, revision.headings, 17),
        (len, revision.media, 18),
        (len, revision.links, 92),
        (len, revision.wikilinks, 76),
        (len, revision.references, 7),
        (len, revision.tags, 1),
    ]:
        assert fn(arg) == ret

    assert isinstance(revision.user, User)
    assert isinstance(revision.page, Page)
    assert isinstance(revision.parent, ParentRevision)


def test_build_revision(revision_data: str) -> None:
    revision_json = json.dumps(revision_data)
    revision = build_current_revision(revision_json)

    for attr, expected in [
        (revision.id, 1234),
        (revision.bytes, 2800),
        (revision.lang, "en"),
        (revision.comment, "Added category baz."),
        (
            revision.timestamp,
            datetime.datetime(2022, 2, 15, 4, 30, 0, tzinfo=datetime.timezone.utc),
        ),
    ]:
        assert attr == expected

    for fn, arg, ret in [
        (len, revision.categories, 2),
        (len, revision.headings, 3),
        (len, revision.media, 0),
        (len, revision.links, 3),
        (len, revision.wikilinks, 1),
        (len, revision.references, 0),
        (len, revision.tags, 0),
    ]:
        assert fn(arg) == ret

    assert isinstance(revision.user, User)
    assert isinstance(revision.page, Page)
    assert isinstance(revision.parent, ParentRevision)


def test_build_revision_missing_properties(revision_data: Dict[str, Any]) -> None:
    properties = list(revision_data.keys())
    for property in properties:
        # remove current property which should raise
        # an error when this data is validated since
        # all properties are required
        value = revision_data.pop(property)
        with pytest.raises(jsonschema.ValidationError):
            build_current_revision(json.dumps(revision_data))

        # replace the removed property
        revision_data[property] = value
