import asyncio
import json
import logging
import re

from dataclasses import dataclass
from datetime import datetime
from typing import List, Optional

import jsonschema  # type: ignore
import mwapi  # type: ignore

from knowledge_integrity import constants
from knowledge_integrity.schema import REVISION_SCHEMA
from knowledge_integrity.utils import parse_timestamp


HEADING_RE = re.compile(
    r"""
    (={2,})  # markup
    (.*?)    # heading
    (={2,})  # markup
    """,
    re.VERBOSE,
)

LINK_RE = re.compile(
    r"""
    (?<=\[\[)  # starts with [[ but not included in match
    .*?        # enclosed text
    (?=]])     # ends with ]] but not included in match
    """,
    re.VERBOSE,
)

MEDIA_NAME_PREFIX = r"""
(?:
      =   # equals sign if in a template
    | :   # or a colon if of the form file:foo.jpg
    | \n  # or newline if in a gallery tag
)
"""
ALLOWED_MEDIA_NAME_CHARACTERS = r"""
    # any character excluding carriage return, newline,
    # hash, angle brackets, square brackets, vertical bar,
    # colon, braces and forward slash
    [^\r\n\#\<\>\[\]\|:\{\}/]
"""
MEDIA_RE = re.compile(
    rf"""
    {MEDIA_NAME_PREFIX}                 # prefix
    ({ALLOWED_MEDIA_NAME_CHARACTERS}+)  # filename
    \.(\w+)                             # extension with period
    """,
    re.VERBOSE,
)

REFERENCE_RE = re.compile(
    r"""
    <ref              # open ref tag
    [^/>]*            # optional reference name i.e. name="foo"
    (?:
          />          # self close if repeated reference
        | >.*?</ref>  # or angle bracket and closing tag with text in between
    )
    """,
    re.IGNORECASE | re.VERBOSE,
)


@dataclass(frozen=True)
class User:
    # id is 0 when the user is anonymous
    id: int
    editcount: int
    groups: List[str]
    # registration timestamp can be None when
    # the user is anonymous
    registration_timestamp: Optional[datetime]

    @property
    def is_anonymous(self) -> bool:
        return self.id == 0


@dataclass(frozen=True)
class Page:
    id: int
    first_edit_timestamp: datetime
    title: str


@dataclass
class Revision:
    id: int
    bytes: int
    comment: str
    text: str
    lang: str
    tags: List[str]
    timestamp: datetime
    _categories = None
    _headings = None
    _links = None
    _media = None
    _references = None
    _wikilinks = None

    @property
    def links(self) -> List[str]:
        if self._links is None:
            # split to get only the title from matches of the form
            # "title | (optional) alias"
            self._links = [
                m.split("|", maxsplit=1)[0] for m in LINK_RE.findall(self.text)
            ]

        return self._links

    @property
    def categories(self) -> List[str]:
        if self._categories is None:
            # split and filter to only get links of the form "Category:Foo"
            split_links = (link.split(":", maxsplit=1)[0] for link in self.links)
            self._categories = [
                link
                for link in split_links
                if link in constants.CATEGORY_PREFIXES
                or link in constants.CATEGORY_ALIASES.get(self.lang, [])
            ]

        return self._categories

    @property
    def headings(self) -> List[str]:
        if self._headings is None:
            # only get headings of level 2 and 3
            self._headings = [
                heading.strip()
                for l_eq, heading, r_eq in HEADING_RE.findall(self.text)
                if l_eq == r_eq and 2 <= len(l_eq) <= 3
            ]

        return self._headings

    @property
    def media(self) -> List[str]:
        if self._media is None:
            self._media = [
                f"{name}.{extension}".strip()
                for name, extension in MEDIA_RE.findall(self.text)
                if extension.lower() in constants.MEDIA_EXTENSIONS
            ]

        return self._media

    @property
    def references(self) -> List[str]:
        if self._references is None:
            self._references = REFERENCE_RE.findall(self.text)

        return self._references

    @property
    def wikilinks(self) -> List[str]:
        if self._wikilinks is None:
            self._wikilinks = [link for link in self.links if ":" not in link]

        return self._wikilinks


@dataclass
class ParentRevision(Revision):
    pass


@dataclass
class CurrentRevision(Revision):
    page: Page
    parent: ParentRevision
    user: User


async def get_user(session: mwapi.AsyncSession, user_id: int) -> Optional[User]:
    response = await session.get(
        action="query",
        formatversion=2,
        list="users",
        usprop="editcount|groups|registration",
        ususerids=user_id,
    )
    logging.debug("User: %d. Response: %r", user_id, response)
    try:
        user = response["query"]["users"][0]
        registration = user.get("registration")
        if registration is not None:
            registration = parse_timestamp(registration)

        return User(
            id=user_id,
            editcount=user.get("editcount", 0),
            groups=user.get("groups", []),
            registration_timestamp=registration,
        )
    except KeyError as e:
        logging.error("Failed to fetch info for user: %d. Reason: %r", user_id, e)
        return None


async def get_page(session: mwapi.AsyncSession, page_id: int) -> Optional[Page]:
    response = await session.get(
        action="query",
        formatversion=2,
        prop="revisions",
        pageids=page_id,
        rvlimit=1,
        rvdir="newer",
        rvslots="main",
    )
    logging.debug("Page: %d. Response: %r", page_id, response)
    try:
        revision = response["query"]["pages"][0]["revisions"][0]
        return Page(
            id=page_id,
            first_edit_timestamp=parse_timestamp(revision["timestamp"]),
            title=response["query"]["pages"][0]["title"],
        )
    except KeyError as e:
        logging.error("Failed to fetch info for page: %d. Reason: %r", page_id, e)
        return None


async def get_parent_revision(
    session: mwapi.AsyncSession, rev_id: int, lang: str
) -> Optional[ParentRevision]:
    response = await session.get(
        action="query",
        formatversion=2,
        prop="revisions",
        revids=rev_id,
        rvprop="content|comment|size|tags|timestamp",
        rvslots="main",
    )
    logging.debug("Parent: %d (%s). Response: %r", rev_id, lang, response)
    try:
        revision = response["query"]["pages"][0]["revisions"][0]
        return ParentRevision(
            id=rev_id,
            comment=revision["comment"],
            bytes=revision["size"],
            lang=lang,
            tags=revision["tags"],
            text=revision["slots"]["main"]["content"],
            timestamp=parse_timestamp(revision["timestamp"]),
        )
    except KeyError as e:
        logging.error(
            "Failed to fetch info for parent: %d (%s). Reason: %r", rev_id, lang, e
        )
        return None


async def get_current_revision(
    session: mwapi.AsyncSession, rev_id: int, lang: str
) -> Optional[CurrentRevision]:
    response = await session.get(
        action="query",
        formatversion=2,
        prop="revisions",
        revids=rev_id,
        rvprop="ids|content|comment|timestamp|size|userid|tags",
        rvslots="main",
    )
    logging.debug("Revision: %d (%s). Response: %r", rev_id, lang, response)
    try:
        page_id = response["query"]["pages"][0]["pageid"]
        revision = response["query"]["pages"][0]["revisions"][0]

        page, parent, user = await asyncio.gather(
            get_page(session, page_id),
            get_parent_revision(session, revision["parentid"], lang),
            get_user(session, revision["userid"]),
        )

        if (page is None) or (parent is None) or (user is None):
            return None

        return CurrentRevision(
            id=rev_id,
            bytes=revision["size"],
            comment=revision["comment"],
            lang=lang,
            page=page,
            parent=parent,
            tags=revision["tags"],
            text=revision["slots"]["main"]["content"],
            timestamp=parse_timestamp(revision["timestamp"]),
            user=user,
        )
    except KeyError as e:
        logging.error(
            "Failed to fetch info for revision: %d (%s). Reason: %r", rev_id, lang, e
        )
        return None


def build_current_revision(json_str: str) -> CurrentRevision:
    """Returns a `CurrentRevision` object by deserializing
    `json_str`, validating it against `revision_schema`
    and mapping the JSON keys to `CurrentRevision` fields.
    """
    rev_data = json.loads(json_str)

    jsonschema.validate(
        instance=rev_data,
        schema=REVISION_SCHEMA,
    )

    parent_rev = ParentRevision(
        id=rev_data["parent_id"],
        bytes=rev_data["parent_bytes"],
        comment=rev_data["parent_comment"],
        text=rev_data["parent_text"],
        lang=rev_data["lang"],
        tags=rev_data["parent_tags"],
        timestamp=parse_timestamp(rev_data["parent_timestamp"]),
    )
    user = User(
        id=rev_data["user_id"],
        editcount=rev_data["user_editcount"],
        groups=rev_data["user_groups"],
        registration_timestamp=parse_timestamp(rev_data["user_registration_timestamp"]),
    )
    page = Page(
        id=rev_data["page_id"],
        title=rev_data["page_title"],
        first_edit_timestamp=parse_timestamp(rev_data["page_first_edit_timestamp"]),
    )

    return CurrentRevision(
        id=rev_data["rev_id"],
        bytes=rev_data["rev_bytes"],
        comment=rev_data["rev_comment"],
        lang=rev_data["lang"],
        page=page,
        parent=parent_rev,
        tags=rev_data["rev_tags"],
        text=rev_data["rev_text"],
        timestamp=parse_timestamp(rev_data["rev_timestamp"]),
        user=user,
    )
