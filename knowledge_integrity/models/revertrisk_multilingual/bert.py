from typing import Any, Dict, List, Sequence, Tuple

import numpy as np
import scipy.special  # type: ignore
import transformers


BERT_MODEL_FEATURES_DEFAULTS = {
    "insert": [-1, -1, -1, -1, -1, -1, -1, -1],
    "change": [-1, -1, -1, -1, -1, -1, -1, -1],
    "title": [-1, -1],
    "comment": [-1, -1, -1, -1],
}


def transformer_prediction_processing(predictions: List[Dict[str, Any]]) -> np.ndarray:
    [[score_no], [score_yes]] = [
        [r["score"] for r in predictions if r["label"] == "LABEL_0"],
        [r["score"] for r in predictions if r["label"] == "LABEL_1"],
    ]
    # hstack to get [score_no, score_yes, prob_no, prob_yes]
    # for every result to later calculate mean and max pooling
    return np.hstack(
        [[score_no, score_yes], scipy.special.softmax([score_no, score_yes])]
    )


def classify_title(model: transformers.Pipeline, title: str) -> Dict[str, float]:
    [result] = model(
        title,
        top_k=2,
        function_to_apply="none",
        truncation=True,
        max_length=512,
        batch_size=1,
    )
    return dict(bert_title_score=result["score"])


def classify_comment(model: transformers.Pipeline, comment: str) -> Dict[str, float]:
    if comment:
        result = model(
            comment,
            top_k=2,
            function_to_apply="none",
            truncation=True,
            max_length=512,
            batch_size=1,
        )
        feature_values = transformer_prediction_processing(result)
    else:
        feature_values = BERT_MODEL_FEATURES_DEFAULTS["comment"]
    labels = [f"bert_comment_{i}" for i in ["s0", "s1", "p0", "p1"]]
    return dict(zip(labels, feature_values))


def classify_inserts(
    model: transformers.Pipeline, inserts: Sequence[str]
) -> Dict[str, float]:
    if len(inserts) > 0:
        results = model(
            inserts,
            top_k=2,
            function_to_apply="none",
            truncation=True,
            max_length=512,
        )
        preds = [transformer_prediction_processing(pred) for pred in results]
        max_pred, mean_pred = np.max(preds, axis=0), np.mean(preds, axis=0)
        feature_values = np.concatenate((max_pred, mean_pred))
    else:
        feature_values = BERT_MODEL_FEATURES_DEFAULTS["insert"]
    labels = [
        f"bert_insert_{value}_{kind}"
        for kind in ("max", "mean")
        for value in ("s0", "s1", "p0", "p1")
    ]
    return dict(zip(labels, feature_values))


def classify_changes(
    model: transformers.Pipeline, changes: Sequence[Tuple[str, str]]
) -> Dict[str, float]:
    if len(changes) > 0:
        formatted_changes = [
            {"text": original, "text_pair": edited} for original, edited in changes
        ]
        results = model(
            formatted_changes,
            top_k=2,
            function_to_apply="none",
            truncation=True,
            max_length=512,
        )
        preds = [transformer_prediction_processing(pred) for pred in results]
        max_pred, mean_pred = np.max(preds, axis=0), np.mean(preds, axis=0)
        feature_values = np.concatenate((max_pred, mean_pred))
    else:
        feature_values = BERT_MODEL_FEATURES_DEFAULTS["change"]
    labels = [
        f"bert_change_{value}_{kind}"
        for kind in ("max", "mean")
        for value in ("s0", "s1", "p0", "p1")
    ]
    return dict(zip(labels, feature_values))
