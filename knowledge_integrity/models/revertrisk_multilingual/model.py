import difflib
import itertools
import logging
import pathlib
import re
import sys

from dataclasses import dataclass
from typing import Any, Dict, List, Sequence, Set, Tuple

import catboost as catb  # type: ignore
import joblib  # type: ignore
import mwedittypes  # type: ignore
import transformers

from fuzzywuzzy import fuzz  # type: ignore

from knowledge_integrity.featureset import (
    ContentFeatures,
    FeatureSource,
    PageFeatures,
    QualityFeatures,
    UserFeatures,
    get_features,
)
from knowledge_integrity.models.revertrisk_multilingual.bert import (
    classify_changes,
    classify_comment,
    classify_inserts,
    classify_title,
)
from knowledge_integrity.revision import CurrentRevision


MODEL_VERSION: int = 1

ALLOWED_REVISION_TAGS = [
    "mobile edit",
    "mobile web edit",
    "visualeditor",
    "wikieditor",
    "mobile app edit",
    "android app edit",
    "ios app edit",
]
ALLOWED_CONTENT_TYPES = [
    "Argument",
    "Category",
    "Comment",
    "ExternalLink",
    "Gallery",
    "HTMLEntity",
    "Heading",
    "List",
    "Media",
    "Paragraph",
    "Punctuation",
    "Reference",
    "Section",
    "Sentence",
    "Table",
    "Table Element",
    "Template",
    "Text",
    "Text Formatting",
    "Whitespace",
    "Wikilink",
    "Word",
]
ALLOWED_ACTION_TYPES = ["change", "insert", "move", "remove"]

CLASSIFIER_INTERMEDIATE_FEATURES = [
    "page_title",
    "comment",
    "wiki_db",
    "revision_text_bytes_diff",
    "is_mobile_edit",
    "is_mobile_web_edit",
    "is_visualeditor",
    "is_wikieditor",
    "is_mobile_app_edit",
    "is_android_app_edit",
    "is_ios_app_edit",
    "texts_removed",
    "texts_insert",
    "texts_change",
    "change_Argument",
    "insert_Argument",
    "move_Argument",
    "remove_Argument",
    "change_Category",
    "insert_Category",
    "move_Category",
    "remove_Category",
    "change_Comment",
    "insert_Comment",
    "move_Comment",
    "remove_Comment",
    "change_ExternalLink",
    "insert_ExternalLink",
    "move_ExternalLink",
    "remove_ExternalLink",
    "change_Gallery",
    "insert_Gallery",
    "move_Gallery",
    "remove_Gallery",
    "change_HTMLEntity",
    "insert_HTMLEntity",
    "move_HTMLEntity",
    "remove_HTMLEntity",
    "change_Heading",
    "insert_Heading",
    "move_Heading",
    "remove_Heading",
    "change_List",
    "insert_List",
    "move_List",
    "remove_List",
    "change_Media",
    "insert_Media",
    "move_Media",
    "remove_Media",
    "change_Paragraph",
    "insert_Paragraph",
    "move_Paragraph",
    "remove_Paragraph",
    "change_Punctuation",
    "insert_Punctuation",
    "move_Punctuation",
    "remove_Punctuation",
    "change_Reference",
    "insert_Reference",
    "move_Reference",
    "remove_Reference",
    "change_Section",
    "insert_Section",
    "move_Section",
    "remove_Section",
    "change_Sentence",
    "insert_Sentence",
    "move_Sentence",
    "remove_Sentence",
    "change_Table",
    "insert_Table",
    "move_Table",
    "remove_Table",
    "change_Table Element",
    "insert_Table Element",
    "move_Table Element",
    "remove_Table Element",
    "change_Template",
    "insert_Template",
    "move_Template",
    "remove_Template",
    "change_Text",
    "insert_Text",
    "move_Text",
    "remove_Text",
    "change_Text Formatting",
    "insert_Text Formatting",
    "move_Text Formatting",
    "remove_Text Formatting",
    "change_Whitespace",
    "insert_Whitespace",
    "move_Whitespace",
    "remove_Whitespace",
    "change_Wikilink",
    "insert_Wikilink",
    "move_Wikilink",
    "remove_Wikilink",
    "change_Word",
    "insert_Word",
    "move_Word",
    "remove_Word",
]


@dataclass
class MultilingualRevertRiskModel:
    model_version: int
    classifier: catb.CatBoostClassifier
    change_model: transformers.Pipeline
    insert_model: transformers.Pipeline
    title_model: transformers.Pipeline
    comment_model: transformers.Pipeline
    supported_wikis: List[str]


@dataclass
class ClassifyResult:
    prediction: bool
    probability: float


@dataclass(frozen=True)
class TextDiff:
    added: List[str]
    deleted: List[str]
    changed: List[Tuple[str, str]]


@dataclass(frozen=True)
class EditInfo:
    actions: Dict[str, int]
    text_diff: TextDiff


def sentence_tokenize(text: str) -> List[str]:
    """
    Basic method used to split text into sentences
    """
    return list(map(str.strip, re.split(r"[.!?](?!$)", text)))


def flatten_list(list_of_items: List[List[Any]]) -> List[Any]:
    """
    Method to flatten the list
    """
    return [item for sublist in list_of_items for item in sublist]


def diff_lines(old: str, new: str) -> TextDiff:
    """Splits `old` and `new` into lines and calculates the
    `TextDiff` for them.
    """
    old_lines, new_lines = old.splitlines(), new.splitlines()
    matcher = difflib.SequenceMatcher(isjunk=None, a=old_lines, b=new_lines)
    added, deleted = set(), set()
    for tag, old_lo, old_hi, new_lo, new_hi in matcher.get_opcodes():
        if tag != "equal":
            added.update(
                flatten_list([sentence_tokenize(s) for s in new_lines[new_lo:new_hi]])
            )
            deleted.update(
                flatten_list([sentence_tokenize(s) for s in old_lines[old_lo:old_hi]])
            )

    changed = []
    seen: Set[str] = set()
    for deleted_line, added_line in itertools.product(deleted, added):
        if deleted_line not in seen and added_line not in seen:
            similarity = fuzz.ratio(deleted_line, added_line)
            if similarity > 60 and deleted_line != added_line:
                changed.append((deleted_line, added_line))
                seen.update((deleted_line, added_line))
            elif deleted_line == added_line:
                seen.update((deleted_line, added_line))

    return TextDiff(
        added=list(added - seen),
        deleted=list(deleted - seen),
        changed=changed,
    )


def get_edit_info(prev_wikitext: str, curr_wikitext: str, lang: str) -> EditInfo:
    """Compares `prev_wikitext` and `curr_wikitext` and returns
    information about the edit that converted the former into the
    latter.
    """
    et = mwedittypes.EditTypes(prev_wikitext, curr_wikitext, lang)
    actions = et.get_diff()
    filtered_actions = {
        f"{at}_{ct}": actions.get(ct, {}).get(at, 0)
        for ct in ALLOWED_CONTENT_TYPES
        for at in ALLOWED_ACTION_TYPES
    }

    lines_added = [
        node["text"] for node in et.tree_diff["insert"] if node["type"] == "Text"
    ]
    lines_deleted = [
        node["text"] for node in et.tree_diff["remove"] if node["type"] == "Text"
    ]
    lines_changed = []
    for node in et.tree_diff["change"]:
        if node["prev"]["type"] == node["curr"]["type"] == "Text":
            change_diff = diff_lines(node["prev"]["text"], node["curr"]["text"])
            lines_added.extend(change_diff.added)
            lines_deleted.extend(change_diff.deleted)
            lines_changed.extend(change_diff.changed)

    return EditInfo(
        actions=filtered_actions,
        text_diff=TextDiff(
            added=lines_added, deleted=lines_deleted, changed=lines_changed
        ),
    )


def one_hot_encode(
    sample: List[str], labels: List[str], prefix: str = "is_"
) -> Dict[str, int]:
    """Returns a dictionary of one hot encoded sample labels."""
    # convert to set so that we can do a membership test in O(1)
    # for each label in sample
    sample_set = set(sample)
    return {
        prefix + label.replace(" ", "_"): 1 if label in sample_set else 0
        for label in labels
    }


def _transformed_revertrisk_features(features: Dict[str, Any]) -> Dict[str, Any]:
    byte_diff = features["revision_text_bytes"] - features["parent_revision_text_bytes"]
    one_hot_tags = one_hot_encode(features["tags"], ALLOWED_REVISION_TAGS)
    edit_info = get_edit_info(
        features["parent_wikitext"],
        features["wikitext"],
        features["wiki_db"].replace("wiki", ""),
    )

    return dict(
        revision_text_bytes_diff=byte_diff,
        texts_removed=edit_info.text_diff.deleted,
        texts_insert=edit_info.text_diff.added,
        texts_change=edit_info.text_diff.changed,
        **edit_info.actions,
        **one_hot_tags,
    )


def extract_features(
    revision: CurrentRevision, features: Sequence[str]
) -> Dict[str, Any]:
    # groups of features required for the current revision
    user_features = UserFeatures(revision)
    page_features = PageFeatures(revision)
    content_features = ContentFeatures(revision)
    quality_features = QualityFeatures(revision)

    # groups of features required for the parent revision
    parent_content_features = ContentFeatures(revision.parent)
    parent_quality_features = QualityFeatures(revision.parent)

    feature_sources = (
        FeatureSource(source=user_features),
        FeatureSource(source=page_features),
        FeatureSource(source=content_features),
        FeatureSource(source=quality_features),
        FeatureSource(source=parent_content_features, prefix="parent_"),
        FeatureSource(source=parent_quality_features, prefix="parent_"),
    )
    return get_features(feature_sources, features, _transformed_revertrisk_features)


def load_model(model_path: pathlib.Path) -> MultilingualRevertRiskModel:
    # ensure that joblib can find the model in global symbols when unpickling
    sys.modules["__main__"].MultilingualRevertRiskModel = MultilingualRevertRiskModel  # type: ignore  # noqa: E501

    with open(model_path, "rb") as f:
        model: MultilingualRevertRiskModel = joblib.load(f)

    if model.model_version != MODEL_VERSION:
        logging.warn(
            "Serialized model version %d does not match current model version %d",
            model.model_version,
            MODEL_VERSION,
        )
    return model


def classify(
    model: MultilingualRevertRiskModel, revision: CurrentRevision
) -> ClassifyResult:
    feature_names = model.classifier.feature_names_
    if feature_names is None:
        raise ValueError("feature_names for serialized classifier cannot be None.")

    intermediate_features = extract_features(revision, CLASSIFIER_INTERMEDIATE_FEATURES)

    # add Bert classification of text elements as features
    features = {
        **classify_title(model.title_model, intermediate_features["page_title"]),
        **classify_comment(model.comment_model, intermediate_features["comment"]),
        **classify_inserts(model.insert_model, intermediate_features["texts_insert"]),
        **classify_changes(model.change_model, intermediate_features["texts_change"]),
        **intermediate_features,
    }

    X = [features[name] for name in feature_names]
    [_, prob_yes] = model.classifier.predict_proba(X)
    return ClassifyResult(
        # convert numpy values to make them serializable
        prediction=bool(prob_yes > 0.5),
        probability=float(prob_yes),
    )
