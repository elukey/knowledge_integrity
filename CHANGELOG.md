# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2022-12-14
### Added
- (#7) Added content oriented Knowledge Integrity model for predicting reverts.

### Changed
- (#7) BREAKING:  `REVISION_SCHEMA` now requires `page_title`, `rev_tags` and `parent_tags`.

## [0.1.1] - 2022-12-14
### Added
- (#5) Package is now `PEP-561` compliant

## [0.1.0] - 2022-10-25
First release.
